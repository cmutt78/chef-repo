fuse Cookbook
=============
This Fuse cookbook will install an early access Red Hat Fuse instance from the public repository and install a basic Fabric 

The end result makes a Fuse instance available at http://server:8181 with login credentials of admin/admin.

Requirements
------------
#### Cookbooks: 
- `ark` - used for tarball retrieval and unzip
- `java` - used to install an oracle distribution version of java


Attributes
----------
TODO: List your cookbook attributes here.

#### fuse::default
- default["java"]["oracle"]["accept_oracle_download_terms"] = true
- default["java"]["java_home"] = "/opt/java"
- default["java"]["jdk_version"] = "7"
- default["application"] = "fuse"
- default["fuse"]["home_dir"] = "/opt"
- default["fuse"]["install"]["dir"] = "/opt/fuse"
- default["fuse"]["install"]["user"] = "apps"
- default["fuse"]["install"]["group"] = "apps"
- default["fuse"]["install"]["home_dir"] = "/home/apps"
- default["fuse"]["install"]["sleep"] = "60"
- default["fuse"]["download"]["url"] = "https://repository.jboss.org/nexus/content/groups/ea/org/jboss/fuse/jboss-fuse-full/6.2.0.redhat-102/jboss-fuse-full-6.2.0.redhat-102.zip"

Usage
-----
#### fuse::default
Just include `fuse` in your node's `run_list`:

```json
{
  "name":"my_node",
  "run_list": [
    "recipe[fuse]"
  ]
}
```

License and Authors
-------------------
#### Authors: 
- Bryan Grant (bryan@corporatemutt.com)
