#
# Cookbook Name:: gerrit
# Recipe:: default
#
# Copyright 2015, CorporateMutt LLC
#
# All rights reserved - Do Not Redistribute
#
include_recipe "java::oracle"

user node["gerrit"]["install"]["user"] do
   home node["gerrit"]["install"]["home_dir"]
end

directory node["gerrit"]["home_dir"] do
    owner node["gerrit"]["install"]["user"]
    group node["gerrit"]["install"]["user"]
    action :create
end

remote_file "#{node["gerrit"]["home_dir"]}/gerrit.war" do
   source node["gerrit"]["download"]["url"]
   owner node["gerrit"]["install"]["user"]
   group node["gerrit"]["install"]["user"]
   mode '0755'
end

execute "init_gerrit_site" do
   command 'java -jar /opt/gerrit/gerrit.war init --batch -d /opt/gerrit/gerrit_testsite'
   user node["gerrit"]["install"]["user"]
   not_if {::File.exists?('/opt/gerrit/gerrit_testsite')}
end

template "/opt/gerrit/gerrit_testsite/etc/gerrit.config" do
    source "gerrit.config.erb"
    mode 00644
    owner node["gerrit"]["install"]["user"]
    group node["gerrit"]["install"]["user"]
end

execute "restart_gerrit_site" do
   command '/opt/gerrit/gerrit_testsite/bin/gerrit.sh restart'
   user node["gerrit"]["install"]["user"]
end
