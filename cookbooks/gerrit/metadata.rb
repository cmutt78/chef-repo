name             'gerrit'
maintainer       'CorporateMutt LLC'
maintainer_email 'bryan@corporatemutt.com'
license          'All rights reserved'
description      'Installs/Configures gerrit'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.0.4'
recipe           'java::oracle', ''

depends "java"
