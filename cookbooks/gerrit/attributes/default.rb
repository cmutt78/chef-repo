default["java"]["oracle"]["accept_oracle_download_terms"] = true
default["java"]["java_home"] = "/opt/java"
default["java"]["jdk_version"] = "7"

default["application"] = "gerrit"
default["gerrit"]["home_dir"] = "/opt/gerrit"
default["gerrit"]["install"]["dir"] = "/opt/gerrit"
default["gerrit"]["install"]["user"] = "gerrit2"
default["gerrit"]["install"]["home_dir"] = "/home/gerrit2"
default["gerrit"]["download"]["url"] = "http://gerrit-releases.storage.googleapis.com/gerrit-2.11.war"

