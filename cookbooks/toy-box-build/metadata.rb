name 'toy-box-build'
maintainer 'Bryan Grant'
maintainer_email 'bryan@corporatemutt.com'
description 'Installs/Configures build server components'
long_description 'Installs/Configures build server components'
version '0.4.1'
supports 'centos'
depends "java"
depends "sonarqube"
depends "jenkins"
depends "artifactory"
depends "maven"
depends "gerrit"
