include_recipe "java::oracle"
include_recipe "sonarqube::default"
include_recipe "jenkins::master"
include_recipe "artifactory"
include_recipe "maven"
include_recipe "gerrit"

jenkins_plugin 'sonar'
jenkins_plugin 'artifactory'
jenkins_plugin 'gerrit'
jenkins_plugin 'gerrit-trigger'

template "/var/lib/jenkins/config.xml" do
  source "config.xml.erb"
  owner "jenkins"
  group "jenkins"
  mode '0644'
end

template "/var/lib/jenkins/hudson.tasks.Maven.xml" do
  source "hudson.tasks.Maven.xml.erb"
  owner "jenkins"
  group "jenkins"
  mode '0644'
end

jenkins_command 'safe-restart'
