default["java"]["oracle"]["accept_oracle_download_terms"] = true
default["java"]["java_home"] = "/opt/java"
default["java"]["jdk_version"] = "7"

default['artifactory']['port'] = "8081"
default['artifactory']['zip_url'] = 'http://dl.bintray.com/content/jfrog/artifactory/artifactory-3.6.0.zip'
default['artifactory']['home'] = '/opt/artifactory'
default['artifactory']['install_java'] = false

default['maven']['m2_home'] = '/opt/maven'
default['maven']['install_java'] = false
