default["java"]["oracle"]["accept_oracle_download_terms"] = true
default["java"]["java_home"] = "/opt/java"
default["java"]["jdk_version"] = "8"

default["application"] = "fuse"
default["fuse"]["home_dir"] = "/opt"
default["fuse"]["install"]["dir"] = "/opt/fuse"
default["fuse"]["install"]["user"] = "apps"
default["fuse"]["install"]["group"] = "apps"
default["fuse"]["install"]["home_dir"] = "/home/apps"
default["fuse"]["install"]["sleep"] = "60"
default["fuse"]["download"]["url"] = "https://repository.jboss.org/nexus/content/groups/ea/org/jboss/fuse/jboss-fuse-full/6.2.1.redhat-178/jboss-fuse-full-6.2.1.redhat-178.zip"

