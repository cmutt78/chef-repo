name             'fuse'
maintainer       'CorporateMutt LLC'
maintainer_email 'bryan@corporatemutt.com'
license          'All rights reserved'
description      'Installs/Configures fuse'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.5.0'
recipe           'java::oracle', ''

depends "java"
depends "ark"
