#
# Cookbook Name:: fuse
# Recipe:: default
#
# Copyright 2015, CorporateMutt LLC
#
# All rights reserved - Do Not Redistribute
#
include_recipe "java::oracle"

user node["fuse"]["install"]["user"] do
   home node["fuse"]["install"]["home_dir"]
end

ark node["application"] do
   path node["fuse"]["home_dir"] 
   url node["fuse"]["download"]["url"]
   owner node["fuse"]["install"]["user"]
   action :put
end

execute "sed_activate_default_user" do
  command "sed -i 's/\#admin/admin/' #{node["fuse"]["install"]["dir"]}/etc/users.properties"
  action :run
end

template "#{node["fuse"]["install"]["dir"]}/create_fabric.sh" do
  source "create_fabric.sh.erb"
  owner node["fuse"]["install"]["user"]
  group node["fuse"]["install"]["group"]
  mode '0755'
end

template "/etc/init.d/#{node["application"]}" do
  source "fuse.erb"
  mode '0755'
  owner 'root'
  group 'root'
end

service node["application"] do
  supports :status => true, :restart => true, :reload => true
  action [ :enable, :start ]
end

execute "sleep" do
  command "sleep #{node["fuse"]["install"]["sleep"]}"
  action :run
end

execute "install fabric" do
  command "sudo su #{node["fuse"]["install"]["user"]} -c \"#{node["fuse"]["install"]["dir"]}/create_fabric.sh\""
  action :run
end
